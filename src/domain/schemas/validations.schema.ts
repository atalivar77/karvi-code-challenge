

import * as Joi from "joi"

const commonValidation = Joi.string().valid("br", "ar").required();

const DataSchema = async (site:any) => {
    const schema = Joi.object({
        site: commonValidation
    })
    try {
        const value = await schema.validateAsync({ site: site });
        return value;
    }
    catch (err) {
        return err;
    }
}

const CarsByIdSchema = async (site:any,ids:any) => {
    const schema = Joi.object({
        site: commonValidation,
        ids: Joi.string()
            .required().regex(/^\d+(,\d+)*$/)
            .prefs({messages:{
                'string.pattern.base':`The format must be a number separated by a comma (example 123,124,125)`
            }})
    })

    try {
        const value = await schema.validateAsync({ site: site, ids: ids });
        return value;
    }
    catch (err) {
        return err;
    }
}




export { DataSchema, CarsByIdSchema } ;



