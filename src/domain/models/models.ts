/* Models */
export class DataTransformation {
    constructor(
        public items?: Array<Car>,
        public filters?: Filter
    ) { }
}

export class Filter {
    constructor(
        public city?: Array<string>,
        public state?: Array<string>,
        public brand?: Array<string>,
        public model?: Array<string>,
    ) { }
}

export class Car {
    constructor(
        public id: number,
        public city: string,
        public state: string,
        public year: string,
        public brand: string,
        public model: string,
        public version: string,
        public price: number,
        public mileage: number,
        public image: string,
        public certificate: boolean,
        public promoted: boolean,
        
    ) { }
}