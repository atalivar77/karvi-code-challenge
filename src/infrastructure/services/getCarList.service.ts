import { config } from '../../config';
import axios, { AxiosRequestConfig } from 'axios';

export default async function getCarList(site: string) {
    const request:AxiosRequestConfig = {
        url: `${config.API_URL}?site=${site}`,
        headers: { 
            'Content-Type': 'application/json',
            'api-key': config.API_KEY as string,
        }
    }
    try {
        return (await axios.request(request)).data;
    } catch (error) {
        return error;
    }
}

