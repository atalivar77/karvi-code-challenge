import { Request, Response } from "express";
import filterById from "../services/cars.by.id.service";
import dataTransform from "../services/data.transform.service";
import { Car } from "../../domain/models/models";

export default {
  dataTransform: async (req: Request, res: Response) => {
    return res.status(200).send(dataTransform(req.body.carsData as Array<Car>, req.query.site as string)); //transform data
  },
  carsByIds: async (req: Request, res: Response) => {
    return res.status(200).send(filterById(req.body.carsData as Array<Car>, req.query.ids as string));
  }
}
