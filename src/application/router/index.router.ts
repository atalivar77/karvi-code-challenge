
import { Application } from "express";
import api from "../controllers/cars.controller";
import getCarsMiddleware from "../middleware/getCars.middleware copy";
import validation from "../middleware/validation.schema.middleware";

export const router = (app: Application): void => {
  //Endpoint 1: Data Transformation 
  app.get("/transformation", validation, getCarsMiddleware, api.dataTransform )
  //Endpoint 2: Cars by Id
  app.get("/cars", validation, getCarsMiddleware, api.carsByIds)
};