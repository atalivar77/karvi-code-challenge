import { Car, DataTransformation } from "../../domain/models/models";
import { dataFilters, sortItems } from "../helpers/data.transform.filters.helper";

//Helper Data Transform
const dataTransform = (carsData:Array<Car>,site:string):DataTransformation => {
    return new DataTransformation(sortItems(carsData,site),dataFilters(carsData));
}
export default dataTransform;