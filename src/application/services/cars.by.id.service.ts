import {Car} from "../../domain/models/models";

//service Find by Id
const filterById = (carsData:Array<Car>,ids:string):Array<Car> => {
    let cars: Array<Car> = [];
    const arrayIds: Array<string> = ids.toString().split(',');
    
    arrayIds.map(id => {
      const found: any = carsData.find((car:Car) =>{if(parseInt(id) === car.id) return car});
      if(found && found.id) cars.push(found);
    })
    return cars;
}

export default filterById;