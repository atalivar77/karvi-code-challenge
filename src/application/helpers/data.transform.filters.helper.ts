import {Car, Filter} from "../../domain/models/models";

const sortItems = (carsData:Array<Car>,site:string):Array<Car> => {
  return carsData.sort((car1:Car,car2:Car) => { 
    return (formatYear(car2.year,site) - formatYear(car1.year,site) || car1.price - car2.price)
  });
}

const formatYear = (year:string,site:string):number => {
  year = site === 'br'? year.split('/')[0] : year;
  return parseInt(year);
}

const createFilter = (key:string,carsData:Array<Car>):Array<string> => {
  const currentFilter:Array<string> = carsData.map((car:any) => {
    return car[key];
  });
  return currentFilter.filter(onlyUnique);
}

const onlyUnique = (value:any, index:any, self:any):boolean => {
  return self.indexOf(value) === index;
}

const dataFilters = (carsData:Array<Car>):Filter => {  
  return new Filter(
    createFilter('city',carsData),
    createFilter('state',carsData),
    createFilter('brand',carsData),
    createFilter('model',carsData),
  ); 
}  

export {sortItems,dataFilters,formatYear,createFilter,onlyUnique}