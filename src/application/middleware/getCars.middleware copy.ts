
import { Request, Response, NextFunction } from "express";
import getCarList from "../../infrastructure/services/getCarList.service";

const getCarsMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  const site:string = req.query.site as string; 
  const carsData: any = await getCarList(site); //service
  if (!carsData || carsData === null) return res.status(500).send({ message: 'server error' })
  if (carsData.isAxiosError) {
    const resp = carsData.response;
    return res.status(resp.status).send({ message: resp.data })
  }
  req.body.carsData = carsData; //add data
  next();
};

export default getCarsMiddleware;