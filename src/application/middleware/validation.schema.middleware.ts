
import { Request, Response, NextFunction } from "express";
import { DataSchema, CarsByIdSchema } from "../../domain/schemas/validations.schema";

const validation = async (req: Request, res: Response, next: NextFunction) => {
  let validation; 
  const isIdCar:Boolean = req.url.toString().includes('cars'); //select schema of validation
  
  if(isIdCar) validation = await CarsByIdSchema(req.query.site,req.query.ids);
  else validation = await DataSchema(req.query.site);
  if (validation.message) return res.status(400).send({ message: validation.message });

  next();
};

export default validation;