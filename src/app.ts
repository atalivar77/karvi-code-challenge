import express from "express";
import { router } from "./application/router/index.router";
import { config } from "./config";

// Create Express server
const app = express();

// Express configuration
app.set("port", config.PORT);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

router(app);

export default app;
