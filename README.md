# Karvi - Code Challenge

## Descripción

En primer lugar se consume un servicio provisto por karvi, el cual trae una lista de autos según el país (Argentina o Brasil), la consigna consiste en crear:
 
1 - Un primer endpoint con transformación de datos, del servicio consumido.
 
2 - Un segundo endpoint que busque autos según sus ids.


## Instalación
***
1 - Clonar el repo.
 
2 - Instalar dependencias
```
npm i
```
3 - Transpilar la aplicación (necesario para obtener el dist)
```
npm run build
```
4 - Iniciar el servidor
```
npm run start
```

### Opcionales

Iniciar el servidor en modo desarrollo

```
npm run dev
```
Ejecutar Pruebas
```
npm run test
```
### Extra 

Agregar el debbug para VsCode

1 - En la carpeta .vscode, crear el archivo launch.json 

2 - Copiar la siguiente configuración
```
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "launch",
            "name": "Debug",
            "protocol": "inspector",
            "cwd": "${workspaceRoot}",
            "runtimeExecutable": "npm",
            "runtimeArgs": [
                "run-script",
                "dev"
            ]
        }
    ]
}
```

***
## Usabilidad

Despues de inicializar el servidor. Ingresar la siguiente dirección

Para el primer endpoint: 

Es necesario incluir el parámetro site indicado si ar o br

```
http://localhost:3000/transformation?site=br
```


Para el segundo endpoint:

Además del parámetro site se agrega ids. Por ejemplo agregando dos ids para buscar dos autos. En caso de no encontrarse el arreglo devuelto sera vacío.

```
http://localhost:3000/cars?site=br&ids=410689,410563
```

***
## Despliegue

Desplegado en heroku

- [Endpoint 1](https://karvi-code-challenge.herokuapp.com/transformation)
- [Endpoint 2](https://karvi-code-challenge.herokuapp.com/cars)

***
## Tecnologías utilizadas

- [Node](https://nodejs.org/es/)
- [TypeScript](https://www.typescriptlang.org/)

***
## Autor
Martin Adrian Balmaceda [Linkedin](https://www.linkedin.com/in/adrianbalmaceda/)

## License
Este proyecto está bajo la Licencia MIT.

