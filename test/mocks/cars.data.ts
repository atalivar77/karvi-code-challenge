import { Car } from "../../src/domain/models/models";

const carsData:Array<Car> = [
  {
    "id": 366383,
    "city": "Araraquara",
    "state": "SP",
    "year": "2021/2022",
    "brand": "FIAT",
    "model": "STRADA",
    "version": "1.3 FIREFLY FLEX FREEDOM CS MANUAL",
    "price": 96900,
    "mileage": 0,
    "image": "https://loja-conectada.s3-sa-east-1.amazonaws.com/fotos/358/146910-fiat-strada-20210921142947714276.jpg",
    "certificate": false,
    "promoted": false
  },
  {
    "id": 410689,
    "city": "Niterói",
    "state": "RJ",
    "year": "2020/2021",
    "brand": "RENAULT",
    "model": "LOGAN",
    "version": "1.0 12V SCE FLEX LIFE MANUAL",
    "price": 56200,
    "mileage": 38920,
    "image": "https://api.mobiauto.com.br/images/api/images/v1.0/94852561",
    "certificate": false,
    "promoted": false
  }
]

export default carsData;