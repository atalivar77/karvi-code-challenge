import request from "supertest";
import app from "../../../src/app";
import carsData from "../../mocks/cars.data";

describe("GET Data Transform", () => {
  test("should return 200 OK Endpoint 1", () => {
    return request(app).get("/transformation?site=ar").expect(200);
  });
  test("should return 200 OK Endpoint 2", () => {
    return request(app).get("/cars?site=br&ids=366383,410689").expect(200);
  });
});

describe("GET Cars by Id", () => {
  test("Test 2 cars", () => {
    return request(app).get("/cars?site=br&ids=366383,410689").expect(carsData);
  });

  test("Test empty Cars", () => {
    return request(app).get("/cars?site=br&ids=123").expect([]);
  });
});