import { not } from "joi";
import { createFilter, dataFilters, formatYear, sortItems } from "../../../src/application/helpers/data.transform.filters.helper";
import carsData from "../../mocks/cars.data";
import expectData from "../../mocks/expect.data";


const filterExpect = {"brand": ["FIAT", "RENAULT"], "city": ["Araraquara", "Niterói"], "model": ["STRADA", "LOGAN"], "state": ["SP", "RJ"]};

describe('Filter Test',() => {
  test('test city filter',() => {
    expect(createFilter('city',carsData)).toEqual(['Araraquara','Niterói']);
  })
  test('test model filter',() => {
    expect(createFilter('model',carsData)).toContain("STRADA");
  })
  test('test state filter Failure',() => {
    expect(createFilter('state',carsData)).not.toContain("CORDOBA");
  })
  test('test state filter',() => {
    expect(createFilter('state',carsData)).toContain("SP");
  })
  test('test brand filter',() => {
    expect(createFilter('brand',carsData)).toContain('FIAT');
  })

  test('Test Data Filters',() => {
    expect(dataFilters(carsData)).toEqual(filterExpect);
  }) 
})

const formtatExpect = 2020;

describe('Sort Data Test',() => {
  test('Test Format Year BR',() => {
    expect(formatYear("2020/2021","br")).toEqual(formtatExpect);
  })
  test('Test Format Year AR',() => {
    expect(formatYear("2020","ar")).toEqual(formtatExpect);
  })

  test('TEST Sort items ',() => {
    expect(sortItems(carsData,"br")).toEqual(expectData);
  })
})

