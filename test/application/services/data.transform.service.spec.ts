import dataTransform from "../../../src/application/services/data.transform.service";
import { DataTransformation } from "../../../src/domain/models/models";
import carsData from "../../mocks/cars.data";

describe('Data Transform Test',() => {
    test('test Data Type',() => {
      expect(dataTransform(carsData,"ar")).toBeInstanceOf(DataTransformation);
    })  
})