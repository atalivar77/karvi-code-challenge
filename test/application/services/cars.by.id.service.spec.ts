import filterById from "../../../src/application/services/cars.by.id.service";
import { Car } from "../../../src/domain/models/models";
import carsData from "../../mocks/cars.data";

const carsEmpty = new Array<Car>();

describe('Filter by Id Test',() => {
  test('test Data Type',() => {
    expect(filterById(carsData,'0,1,366383')).toEqual([carsData[0]]);
  })
  
})